# Sage Code Challenge

Using the file "files/Code test.xls", create REST API (any language of choice) with following requirements. You can create JSON from the file and pull the data from it.  

# Requirements: 
* Application should build, run and function as expected. 
* Application should return JSON response when ID parameter is passed: 
	Eg. "http://localhost?id=322" should return following result: 
```
	{
	  "id": "322",
	  "result": [
		{
		  "category": "...",
		  "risk_profile": "...",
		  "name": "...",
		  "naics_code": "...",
		  "iso_gl_codes": "...",
		  "wc_codes": "...",
		  "min_coverage": "...",
		  "other_coverage": "..."
		}
	  ]
	}
```

# Submission
* Either Fork this repository and create a Pull Request with your code for review along with with instructions on how to run/build the solution.
* Or send via zip file with instruction on how to run/build the solution. 

# Time
Estimated 4-5 hours

# Notes
Feel free to use an existing framework. 

# Extra Credit

* Create different endpoints for API.

