<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Excel;
use App\Imports\DataImport;

class ExcelController extends Controller
{
	public function index($id)
	{
		$rows = Excel::toArray(new DataImport, 'C:\xampp\htdocs\data\files\Code_test.xlsx');
		//dd($rows);
		$formattedData = [];
		$fields = $rows[0][0];
		unset($rows[0][0]);
		

		// $string = "{" ;

		foreach ($rows[0] as $row) {
			if($row[2] == $id) {
				foreach ($row as $key=>$data) {

				$x[$fields[$key]] = $data;
				logger($data);

			}
			$formattedData[]=$x;
			}
			
		}
		// dd($formattedData);

		return response()->json(['id' => $id, 'result' =>$formattedData]);
	}
}
