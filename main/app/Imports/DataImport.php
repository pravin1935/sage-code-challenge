<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;

class DataImport implements ToCollection
{
    /**
    * @param Collection $collection
    */
    public function collection(Collection $rows)
    {
        $formattedData = [];
		$fields = $rows->toArray()[0];
		unset($rows[0]);
		

		// $string = "{" ;

		foreach ($rows as $row) {

			foreach ($row as $key=>$data) {

				$x[$fields[$key]] = $data;
				logger($data);

			}
			$formattedData[]=$x;
			
		}
		logger($formattedData);
		// session(['key' => $formattedData]);

		return($formattedData);

		// for ($i =1 ; $i<count($rows); $i++)
		// {
		// 	for ($j=0; $j<count($rows[0]) ; $j++ )
				 
		// 		 $rows[0][$j] ;
					
		// 		// $string = $string.$rows[$i][$j] ;

		// }
		// Log::info($string);

    }
}
